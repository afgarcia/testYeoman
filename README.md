# test-yeoman

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1.

This project is a test, about how to use Yeoman to create awesome web apps, this repository  include the dist folder, ready to deploy

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
